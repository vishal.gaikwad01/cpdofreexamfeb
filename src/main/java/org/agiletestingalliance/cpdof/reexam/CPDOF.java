package org.agiletestingalliance.cpdof.reexam;

public class CPDOF {
	
	public String aboutCPDDof() {
		String about = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.\"";
		
		return about;
	}
	
	public int max(int i, int j) {
		
		if (i > j)
			return i;
		else
			return j;
	}

}
